import datetime
import json
import requests
import pprint
import sys
import re

from eprint import eprint


CREATE_FILES = False


# Check the arguments given to this script:
if len(sys.argv) == 2 and (sys.argv[1] == "-h" or sys.argv[1] == "--help"):
    print("NFL game grapper for pro7maxx")
    print(f"\tUsage: {sys.argv[0]} [-h/--help] for help")
    print(f"\tUsage: {sys.argv[0]} [yyyy-mm-dd] for games that day")
    print(f"\tUsage: {sys.argv[0]} for games today")
    print(f"\tUsage: Add '--create' to create files named as the found games")
    print("\tFound games will be printed in ascending order sorted by time")
    sys.exit(0)

if len(sys.argv) >= 2 and (re.match(r"\d{4}-\d{2}-\d{2}", sys.argv[1])):
    date = sys.argv[1]
    year, month, day = date.split("-")

    if len(year) != 4 or len(month) != 2 or len(day) != 2:
        eprint("Date format is not correct! Enter leading zeroes too!")
        sys.exit(-1)
if "--create" in sys.argv:
    CREATE_FILES = True

if len(sys.argv) >= 2:
    date = sys.argv[1]
else:
    date = datetime.datetime.now().strftime("%Y-%m-%d")
    print(f"Using current date: {date}")

# URL to request. Todo: Make requested date chosable. See data=2018-10-07 in url
page = f"""https://magellan-api.p7s1.io/epg-broadcast/prosiebenmaxx.de/graphql?operationName=&query=%20query%20EpgQuery(%24domain%3A%20String!%2C%20%24type%3A%20EpgType!%2C%20%24date%3A%20DateTime)%20%7B%20site(domain%3A%20%24domain)%20%7B%20epg(type%3A%20%24type%2C%20date%3A%20%24date)%20%7B%20items%20%7B%20...fEpgItem%20tvShowTeaser(timeSlot%3A%20PRIMETIME)%20%7B%20...fTeaserItem%20%7D%20episodeTeaser(timeSlot%3A%20PRIMETIME)%20%7B%20...fTeaserItem%20%7D%20filmTeaser(timeSlot%3A%20PRIMETIME)%20%7B%20...fTeaserItem%20%7D%20%7D%20%7D%20%7D%20%7D%0Afragment%20fEpgItem%20on%20EpgItem%20%7B%20id%20type%20currentSite%20title%20description%20startTime%20endTime%20episode%20%7B%20number%20%7D%20season%20%7B%20number%20%7D%20tvShow%20%7B%20title%20%7D%20images%20%7B%20url%20title%20copyright%20%7D%20links%20%7B%20href%20contentType%20title%20%7D%20%7D%20%0Afragment%20fTeaserItem%20on%20TeaserItem%20%7B%20id%20url%20info%20headline%20contentType%20channel%20%7B%20...fChannelInfo%20%7D%20branding%20%7B%20...fBrand%20%7D%20site%20image%20videoType%20orientation%20date%20flags%20valid%20%7B%20from%20to%20%7D%20epg%20%7B%20episode%20%7B%20...fEpisode%20%7D%20season%20%7B%20...fSeason%20%7D%20duration%20nextEpgInfo%20%7B%20...fEpgInfo%20%7D%20%7D%20%7D%20%0Afragment%20fChannelInfo%20on%20ChannelInfo%20%7B%20title%20shortName%20cssId%20cmsId%20%7D%20%0Afragment%20fBrand%20on%20Brand%20%7B%20id%2C%20name%20%7D%20%0Afragment%20fEpisode%20on%20Episode%20%7B%20number%20%7D%20%0Afragment%20fSeason%20on%20Season%20%7B%20number%20%7D%20%0Afragment%20fEpgInfo%20on%20EpgInfo%20%7B%20time%20endTime%20primetime%20%7D%20&variables=%7B%22date%22%3A%22{date}T00%3A00%3A00.000Z%22%2C%22domain%22%3A%22prosiebenmaxx.de%22%2C%22type%22%3A%22FULL%22%7D&queryhash=47a04971e5988e359c28866c6f20afb5845e20c6ce3642cdbb2634916467bad5&initialcv=browser-f3d7e971d93648bea3c8-1"""

# Header parameters, might not be used.
header = """-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0' -H 'Accept: */*' -H 'Accept-Language: de,en-US;q=0.7,en;q=0.3' --compressed -H 'Referer: https://www.prosiebenmaxx.de/tv-programm' -H 'Origin: https://www.prosiebenmaxx.de' -H 'Connection: keep-alive' -H 'DNT: 1'
"""

# command passed to subprocess to get the API answer
command = ["curl", page]

# Get the API response.
page = requests.get(page)
if page.status_code != requests.codes.ok:
    eprint("Request failed!")
    sys.exit(-1)
content = page.content

# API output is JSON, as this is python we use the built-in dict.
dictdump = json.loads(content)

# Get the needed info (TV shows of requested day). Travel through the dict.
tv_shows = dictdump["data"]["site"]["epg"]["items"]

# Filter out all shows that do not contain "ran Football"
football = [x for x in tv_shows if "ran Football" in x["tvShow"]["title"]]
if not football:
    eprint("No games found!")
    sys.exit(0)
#pprint.pprint(football)

# Only thing we need is the show title and the time it is on-air.
football = [{"title": x["tvShow"]["title"], "time": x["startTime"]} for x in football]
#pprint.pprint(football)

# Sort the resulting list by on-air-time ascending (early time first)
football = sorted(football, key=lambda k: k["time"])
#pprint.pprint(football)

# title now should equal something like: "ran Football: Los Angeles Rams at Seattle Seahawks"
# At this time we only need the titles as they were sorted in the previous step.
football = [x["title"] for x in football]
#print(football)

# Output the resulting file name
for index, title in enumerate(football):
    title = title.replace("ran Football: ", "")
    title = f"{index + 1} {title}.ts"
    print(title)
    if CREATE_FILES:
        open(title, 'a').close()
